package com.dexter.qmock.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Directives.{as, complete, entity, get, parameterMap, path, post, _}
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.dexter.qmock.rest.process._
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.immutable._

class QuickMockController(processor: GenericRestProcessor) {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  lazy val corsSettings: CorsSettings =
    CorsSettings.defaultSettings.withAllowedMethods(Seq(OPTIONS, POST, PUT, GET, DELETE))

  def route(implicit system: ActorSystem, m: ActorMaterializer): Route = cors(corsSettings) {
    path("qm" / Segments) {
      segments =>

        get {
          parameterMap { params =>

            entity(as[HttpRequest]) { req =>
              val resp = Validator >> segments match {
                  case Right(t) => processor >> (t._1, t._2, req, params, segments.drop(2))
                  case Left(badResp) => badResp
              }
              complete(resp)
            }
          }
        } ~ post {
          parameterMap { params =>

            entity(as[HttpRequest]) { req =>
              val resp = Validator >> segments match {
                case Right(t) => processor >> (t._1, t._2, req, params, segments.drop(2))
                case Left(badResp) => badResp
              }
              complete(resp)
            }
          }
        } ~ put {
          parameterMap { params =>

            entity(as[HttpRequest]) { req =>
              val resp = Validator >> segments match {
                case Right(t) => processor >> (t._1, t._2, req, params, segments.drop(2))
                case Left(badResp) => badResp
              }
              complete(resp)
            }
          }
        } ~ delete {
          parameterMap { params =>

            entity(as[HttpRequest]) { req =>
              val resp = Validator >> segments match {
                case Right(t) => processor >> (t._1, t._2, req, params, segments.drop(2))
                case Left(badResp) => badResp
              }
              complete(resp)
            }
          }
        }

      }
  }

}
