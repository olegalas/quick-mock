package com.dexter.qmock.rest.process

import akka.http.scaladsl.model.{HttpEntity, HttpResponse, StatusCodes}
import com.dexter.qmock.model.ParametersMismatch
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

object ResponseMaker {

  def internalError(message: String): HttpResponse = {
    HttpResponse(
      status = StatusCodes.InternalServerError,
      entity = HttpEntity(s"Sorry service unavailable now. Reason : $message")
    )
  }

  def specNotFoundResponse(specId: String): Future[HttpResponse] = {
    Future {
      HttpResponse(
        status = StatusCodes.BadRequest,
        entity = HttpEntity(s"We did'n find your configuration by specId : $specId . Ensure that you are registered user.")
      )
    }
  }

  def scenarioNotFoundResponse(scenarioId: String): Future[HttpResponse] = {
    Future {
      HttpResponse(
        status = StatusCodes.BadRequest,
        entity = HttpEntity(s"We did'n find in your configuration specific scenario by id : $scenarioId . Ensure that you have already created this scenario.")
      )
    }
  }

  def scenarioIdNotFoundResponse: Future[HttpResponse] = {
    Future {
      HttpResponse(
        status = StatusCodes.BadRequest,
        entity = HttpEntity("ScenarioId (second segment) was not set.")
      )
    }
  }

  def specIdNotFoundResponse: Future[HttpResponse] = {
    Future {
      HttpResponse(
        status = StatusCodes.BadRequest,
        entity = HttpEntity("SpecificationId (first segment, your login) was not set.")
      )
    }
  }

  def makeUsualResponse(failParameters: Future[List[ParametersMismatch]], successBody: String): Future[HttpResponse] = {
    /*

Lack of parameters:
Method :
  expected (PUT) - was (POST)
Segments :
  expected (seg1) - was (seg2)
  expected (seg3) - was (nothing)
Parameters :
  expected (param1=value1) - was (nothing)
  expected (param2=value1) - was (param2=value2)
Headers :
  expected (param1=value1) - was (nothing)
  expected (param2=value1) - was (param2=value2)
Body :
  expected (body) - was (body)

*/
    failParameters.map {
      case Nil =>
        HttpResponse(
          status = StatusCodes.OK,
          entity = HttpEntity(successBody)
        )
      case fp =>
        val result = fp.map(param => {
          param.parameterName + " :\n" + param.matches.map(m => {
            "expected (" + m.expected + ") - was (" + m.was + ")"
          })
        })
        HttpResponse(
          status = StatusCodes.BadRequest,
          entity = HttpEntity("Lack of parameters:\n" + result)
        )
    }
  }

}
