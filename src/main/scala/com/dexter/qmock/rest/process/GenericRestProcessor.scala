package com.dexter.qmock.rest.process

import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.dexter.qmock.clients.SpecMockClient
import com.dexter.qmock.model._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class GenericRestProcessor(smClient: SpecMockClient) extends RestProcessor {
  override def >>(specId: String,
                  scenarioId: String,
                  req: HttpRequest,
                  params: Map[String, String],
                  segments: List[String]
                      )
                 (implicit mat: ActorMaterializer): Future[HttpResponse] = {

    LOG.info(s"Was received req.\n\tspecId : $specId\n\tscenarioId : $scenarioId\n\tsegments : $segments\n\tparams : $params\n\tmethod : ${req.method}\n\theaders : ${req.headers}\n\tbody : ${req.entity}")

    smClient >> specId flatMap {

      case Right(spec) =>

          spec.scenario.find(sc => sc.scenarioId == scenarioId).map(scenario => {

            val failMismatches = for {
              m <- checkMethod(req, scenario)
              s <- checkSegments(segments, scenario)
              p <- checkParams(params, scenario)
              h <- checkHeaders(req, scenario)
              b <- checkBody(req, scenario)
            } yield m ++ s ++ p ++ h ++ b

            ResponseMaker.makeUsualResponse(failMismatches, scenario.responseBody.getOrElse(""))

          }) getOrElse ResponseMaker.scenarioNotFoundResponse(scenarioId)

      case Left(_) =>
        ResponseMaker.specNotFoundResponse(specId)

    } recover {
      case t:Throwable =>
        LOG.error("Internal server error", t)
        ResponseMaker.internalError(t.getMessage)
    }

  }

  def checkMethod(req: HttpRequest, scenario: Scenario): Future[List[ParametersMismatch]] = {
    Future {
      val expectedMethod = scenario.method
      val wasMethod = req.method.value
      if(expectedMethod.equalsIgnoreCase(wasMethod)) Nil else List(ParametersMismatch("Method", List(Matches(expectedMethod, wasMethod))))
    }
  }

  def checkSegments(segments: List[String], scenario: Scenario): Future[List[ParametersMismatch]] = {
    Future {
      val expectedSegments = scenario.segments
      val wasSegments = segments
      expectedSegments match {
        case Nil => Nil
        case _ =>
          val mismatches = for {
            i <- expectedSegments.indices
            if expectedSegments(i) != wasSegments.lift(i).getOrElse("")
          } yield {
            Matches(expectedSegments(i), segments.lift(i).getOrElse("Was not set"))
          }
          if(mismatches.isEmpty)
            Nil
          else
            List(ParametersMismatch("Segments", mismatches.toList))
      }
    }
  }

  def checkParams(params: Map[String, String], scenario: Scenario): Future[List[ParametersMismatch]] = {
    Future {
      val expectedParams = scenario.params
      val wasParams = params
      expectedParams.isEmpty match {
        case true => Nil
        case _ =>
          val mismatches = expectedParams
            .filterNot(t => wasParams.get(t._1).contains(t._2))
            .map(t => {
              wasParams.get(t._1) match {
                case Some(v) => Matches(t._1 + "=" + t._2, t._1 + "=" + v)
                case None => Matches(t._1 + "=" + t._2, "nothing")
              }
            })
          if (mismatches.isEmpty) Nil
          else List(ParametersMismatch("QueryParams", mismatches.toList))
      }
    }
  }

  def checkHeaders(req: HttpRequest, scenario: Scenario): Future[List[ParametersMismatch]] = {
    Future {
      val expectedHeaders = scenario.headers
      val wasHeaders = req.headers
      expectedHeaders.isEmpty match {
        case true => Nil
        case _ =>
          val mismatches = expectedHeaders
            .filterNot(t => wasHeaders.exists(h => h.name() == t._1 && h.value() == t._2))
            .map(t => {
              wasHeaders
                .filter(h => h.name() == t._1)
                .map(_.value()).headOption match {
                case Some(v) => Matches(t._1 + "=" + t._2, t._1 + "=" + v)
                case None => Matches(t._1 + "=" + t._2, "nothing")
              }
            })
          if (mismatches.isEmpty) Nil
          else List(ParametersMismatch("Headers", mismatches.toList))
      }
    }
  }

  def checkBody(req: HttpRequest, scenario: Scenario)
               (implicit m: ActorMaterializer): Future[List[ParametersMismatch]] = {
    // TODO: Implement JSON and XML check.. not only plain text .... and some regex in future

    scenario.body match {
      case Some(expectedBody) =>

        if(req.entity.contentLengthOption.contains(0L)) {
          Future(List(ParametersMismatch("Body", List(Matches(expectedBody, "<empty>")))))
        } else {
          Unmarshal(req.entity).to[String].map(wasBody => {
            if(expectedBody == wasBody) Nil else List(ParametersMismatch("Body", List(Matches(expectedBody, wasBody))))
          })
        }

      case None =>
        Future(Nil)
    }

  }

}
