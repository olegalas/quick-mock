package com.dexter.qmock.rest.process

import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.ActorMaterializer
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future

trait RestProcessor {
  val LOG: Logger = LoggerFactory.getLogger(this.getClass)
  def >>(specId: String, scenarioId: String, req: HttpRequest, params: Map[String, String], segments: List[String])
        (implicit m: ActorMaterializer): Future[HttpResponse]
}
