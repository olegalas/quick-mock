package com.dexter.qmock.rest

import akka.http.scaladsl.model.HttpResponse
import com.dexter.qmock.rest.process.ResponseMaker

import scala.concurrent.Future

object Validator {

  def >> (segments: List[String]) :Either[Future[HttpResponse], (String, String)] = {
    segments match {
        // first segment is non exit
        case Nil => Left(ResponseMaker.specIdNotFoundResponse)
        // second segment is non exist
        case _ :: Nil => Left(ResponseMaker.scenarioIdNotFoundResponse)
        // everything is ok.. we got two first segments
        case h :: t => Right((h, t.head))
    }
  }

}
