package com.dexter.qmock

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.dexter.qmock.clients.{SpecMockClient, SpecMockClientDefault}
import com.dexter.qmock.rest.QuickMockController
import com.dexter.qmock.rest.process.GenericRestProcessor
import org.slf4j.{Logger, LoggerFactory}
import com.typesafe.config._

import scala.concurrent.ExecutionContextExecutor

object Launch extends App {

  val LOG: Logger = LoggerFactory.getLogger(Launch.getClass)

  implicit val system: ActorSystem = ActorSystem("quick-mock-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val smClient: SpecMockClient = new SpecMockClientDefault(system.settings.config.getString("akka.sm.address"))
  val processor: GenericRestProcessor = new GenericRestProcessor(smClient)
  val controller: QuickMockController = new QuickMockController(processor)
  val bindingFuture = Http().bindAndHandle(controller.route, "localhost", 8080)

  val config = ConfigFactory.load()

  bindingFuture
    .map(binding => {
      LOG.info("Server was run")
      LOG.info(s"Address ${binding.localAddress}")
    }).recover {
    case t:Throwable =>
      LOG.error("There was an error during starting server", t)
      System.exit(-1)
  }


}
