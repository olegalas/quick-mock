package com.dexter.qmock.model

import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import io.circe.Decoder
import io.circe.generic.semiauto._
import io.circe.parser._

import scala.concurrent.Future

case class QuickMockSpecification(
                                   specId:    String,
                                   scenario:  List[Scenario]
                                 )
object QuickMockSpecification {

  private def jsonContentTypes: List[ContentTypeRange] =
    List(`application/json`)

  implicit val decodSpec: Decoder[QuickMockSpecification] = deriveDecoder

  implicit val unmarshSpec: FromEntityUnmarshaller[QuickMockSpecification] = {
    Unmarshaller.stringUnmarshaller
      .forContentTypes(jsonContentTypes: _*)
      .flatMap { ctx => mat => json =>
        decode[QuickMockSpecification](json).fold(Future.failed, Future.successful)
      }
  }


}
case class Scenario(
                   scenarioId:    String,
                   method:        String,
                   segments:      List[String],
                   params:        Map[String, String],
                   headers:       Map[String, String],
                   body:          Option[String],
                   responseBody:  Option[String]
                   )
object Scenario {
  implicit val decodScenario: Decoder[Scenario] = deriveDecoder
}