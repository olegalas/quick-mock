package com.dexter.qmock.model

case class ParametersMismatch(parameterName: String, matches: List[Matches])
case class Matches(expected: String, was: String)