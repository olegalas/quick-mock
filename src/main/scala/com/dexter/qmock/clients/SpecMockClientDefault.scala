package com.dexter.qmock.clients

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import com.dexter.qmock.model.{QuickMockSpecification => Spec}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

final class SpecMockClientDefault(smAddress: String)(implicit system: ActorSystem, mat: Materializer) extends SpecMockClient {

  private val LOG: Logger = LoggerFactory.getLogger(this.getClass)
  private val http = Http()

  def >>(specId: String): Future[Either[Unit ,Spec]] = {

    http.singleRequest(HttpRequest(uri = smAddress + specId))
      .flatMap(resp => {
        if(resp.status.isSuccess())
          Unmarshal(resp.entity).to[Spec].map(spec => Right(spec))
        else
          LOG.info(s"Spec by $specId was not found")
          Future {
            Left()
          }
      })
  }

}
