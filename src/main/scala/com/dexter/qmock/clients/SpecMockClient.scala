package com.dexter.qmock.clients

import scala.concurrent.Future
import com.dexter.qmock.model.{QuickMockSpecification => Spec}

trait SpecMockClient {
  def >>(specId: String): Future[Either[Unit ,Spec]]
}
