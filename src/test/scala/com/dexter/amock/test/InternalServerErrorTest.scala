package com.dexter.amock.test

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.dexter.qmock.clients.SpecMockClient
import com.dexter.qmock.rest.QuickMockController
import com.dexter.qmock.rest.process.GenericRestProcessor
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future


class InternalServerErrorTest extends WordSpec with Matchers with ScalatestRouteTest {

  val smClient: SpecMockClient = (_: String) => {
    Future {
      throw new java.net.ConnectException("Tcp command [Connect(localhost:8081,None,List(),Some(10 seconds),true)] failed because of java.net.ConnectException: Connection refused")
    }
  }

  val processor = new GenericRestProcessor(smClient)
  val controller = new QuickMockController(processor)

  "SpeckMockClient" should {
    "ConnectException" in {
      Get("/qm/speckId/scenarioId") ~> controller.route ~> check {
        status.intValue() should be(500)
        responseAs[String] shouldEqual s"Sorry service unavailable now. Reason : Tcp command [Connect(localhost:8081,None,List(),Some(10 seconds),true)] failed because of java.net.ConnectException: Connection refused"
      }
    }
  }

}
