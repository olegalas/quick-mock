package com.dexter.amock.test

import akka.http.scaladsl.model.headers.RawHeader
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.dexter.qmock.clients.SpecMockClient
import com.dexter.qmock.rest.QuickMockController
import com.dexter.qmock.rest.process.GenericRestProcessor
import com.dexter.qmock.model.{Scenario, QuickMockSpecification => Spec}

import scala.concurrent.Future


class ApiTests extends WordSpec with Matchers with ScalatestRouteTest {

  val specId = "specId"

  val defaultSpec = Spec(specId, List(
    Scenario(
      "scenarioId_1",
      "put",
      List(),
      Map(),
      Map(),
      None,
      Some("response body")
    ),
    Scenario(
      "scenarioId_2",
      "put",
      List("seg_1", "seg_2"),
      Map(),
      Map(),
      None,
      Some("response body")
    ),
    Scenario(
      "scenarioId_3",
      "put",
      List(),
      Map("param1" -> "val1", "param2" -> "val2"),
      Map(),
      None,
      Some("response body")
    ),
    Scenario(
      "scenarioId_4",
      "put",
      List(),
      Map(),
      Map("hed1" -> "val1", "hed2" -> "val2"),
      None,
      Some("response body")
    ),
    Scenario(
      "scenarioId_5",
      "put",
      List(),
      Map(),
      Map(),
      Some("body"),
      Some("response body")
    )
  ))

  val smClient: SpecMockClient = (specId: String) => {
    if(specId == "specId")
      Future {
        Right(defaultSpec)
      }
    else
      Future(Left())
  }

  val processor = new GenericRestProcessor(smClient)
  val controller = new QuickMockController(processor)


  "Basic" should {
    "Ok" in {
      Put("/qm/" + specId + "/scenarioId_1") ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual s"response body"
      }
    }
    "Wrong specId" in {
      val wrongSpecId = "wrongSpecId"
      Put("/qm/" + wrongSpecId + "/scenarioId_1") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"We did'n find your configuration by specId : $wrongSpecId . Ensure that you are registered user."
      }
    }
    "Wrong scenarioId" in {
      val wrongScenarioId = "wrongScenarioId"
      Put("/qm/" + specId + "/" + wrongScenarioId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"We did'n find in your configuration specific scenario by id : $wrongScenarioId . Ensure that you have already created this scenario."
      }
    }
    "SpecId is empty" in {
      Put("/qm/") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual "SpecificationId (first segment, your login) was not set."
      }
    }
    "ScenarioId is empty" in {
      Put("/qm/" + specId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual "ScenarioId (second segment) was not set."
      }
    }
  }

  "Segments" should {
    "Ok" in {
      Put("/qm/" + specId + "/scenarioId_2/seg_1/seg_2") ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual s"response body"
      }
    }
    "Wrong first segment" in {
      val wrongParam = "wrong_seg_1"
      Put("/qm/" + specId + "/scenarioId_2/" + wrongParam + "/seg_2") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Segments :
             |List(expected (seg_1) - was ($wrongParam)))""".stripMargin
      }
    }
    "Wrong second segment" in {
      val wrongParam = "wrong_seg_1"
      Put("/qm/" + specId + "/scenarioId_2/seg_1/" + wrongParam) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Segments :
             |List(expected (seg_2) - was ($wrongParam)))""".stripMargin
      }
    }
    "Empty all segments" in {
      Put("/qm/" + specId + "/scenarioId_2") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Segments :
             |List(expected (seg_1) - was (Was not set), expected (seg_2) - was (Was not set)))""".stripMargin
      }
    }
    "Empty second segments" in {
      Put("/qm/" + specId + "/scenarioId_2/seg_1") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Segments :
             |List(expected (seg_2) - was (Was not set)))""".stripMargin
      }
    }
    "Empty second and wrong first segments" in {
      val wrongParam = "wrong_seg_1"
      Put("/qm/" + specId + "/scenarioId_2/" + wrongParam) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Segments :
             |List(expected (seg_1) - was ($wrongParam), expected (seg_2) - was (Was not set)))""".stripMargin
      }
    }

  }

  "Methods" should {
    "Wrong method" in {
      Delete("/qm/" + specId + "/scenarioId_1") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
            |List(Method :
            |List(expected (put) - was (DELETE)))""".stripMargin
      }
    }
  }

  "Params" should {
    "Ok" in {
      Put("/qm/" + specId + "/scenarioId_3?param1=val1&param2=val2") ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual s"response body"
      }
    }
    "Wrong param1" in {
      val wrongParam = "bla"
      Put("/qm/" + specId + "/scenarioId_3?param1=" + wrongParam + "&param2=val2") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
            |List(QueryParams :
            |List(expected (param1=val1) - was (param1=$wrongParam)))""".stripMargin
      }
    }
    "Wrong param2" in {
      val wrongParam = "bla"
      Put("/qm/" + specId + "/scenarioId_3?param1=val1&param2=" + wrongParam) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(QueryParams :
             |List(expected (param2=val2) - was (param2=$wrongParam)))""".stripMargin
      }
    }
    "Empty param1" in {
      Put("/qm/" + specId + "/scenarioId_3?param2=val2") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(QueryParams :
             |List(expected (param1=val1) - was (nothing)))""".stripMargin
      }
    }
    "Empty param2" in {
      Put("/qm/" + specId + "/scenarioId_3?param1=val1") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(QueryParams :
             |List(expected (param2=val2) - was (nothing)))""".stripMargin
      }
    }
    "Wrong both" in {
      val wrongParam = "bla"
      Put("/qm/" + specId + "/scenarioId_3?param1=" + wrongParam + "&param2=" + wrongParam) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(QueryParams :
             |List(expected (param1=val1) - was (param1=$wrongParam), expected (param2=val2) - was (param2=$wrongParam)))""".stripMargin
      }
    }
    "Empty both" in {
      Put("/qm/" + specId + "/scenarioId_3") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(QueryParams :
             |List(expected (param1=val1) - was (nothing), expected (param2=val2) - was (nothing)))""".stripMargin
      }
    }
    "First wrong second empty" in {
      val wrongParam = "bla"
      Put("/qm/" + specId + "/scenarioId_3?param1=" + wrongParam) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(QueryParams :
             |List(expected (param1=val1) - was (param1=$wrongParam), expected (param2=val2) - was (nothing)))""".stripMargin
      }
    }
  }

  "Headers" should {
    "Ok" in {
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed1", "val1") ~> RawHeader("hed2", "val2") ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual s"response body"
      }
    }
    "Wrong header1" in {
      val wrongHeader = "WRONG"
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed1", wrongHeader) ~> RawHeader("hed2", "val2") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed1=val1) - was (hed1=$wrongHeader)))""".stripMargin
      }
    }
    "Wrong header2" in {
      val wrongHeader = "WRONG"
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed1", "val1") ~> RawHeader("hed2", wrongHeader) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed2=val2) - was (hed2=$wrongHeader)))""".stripMargin
      }
    }
    "Empty header1" in {
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed2", "val2") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed1=val1) - was (nothing)))""".stripMargin
      }
    }
    "Empty header2" in {
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed1", "val1") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed2=val2) - was (nothing)))""".stripMargin
      }
    }
    "Wrong both" in {
      val wrongHeader = "WRONG"
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed1", wrongHeader) ~> RawHeader("hed2", wrongHeader) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed1=val1) - was (hed1=$wrongHeader), expected (hed2=val2) - was (hed2=$wrongHeader)))""".stripMargin
      }
    }
    "Empty both" in {
      Put("/qm/" + specId + "/scenarioId_4") ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed1=val1) - was (nothing), expected (hed2=val2) - was (nothing)))""".stripMargin
      }
    }
    "First wrong second empty" in {
      val wrongHeader = "WRONG"
      Put("/qm/" + specId + "/scenarioId_4") ~> RawHeader("hed1", wrongHeader) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Headers :
             |List(expected (hed1=val1) - was (hed1=$wrongHeader), expected (hed2=val2) - was (nothing)))""".stripMargin
      }
    }
  }

  "Body" should {
    "Ok" in {
      Put("/qm/" + specId + "/scenarioId_5", "body") ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual s"response body"
      }
    }
    "Wrong body" in {
      val wrongBody = "wrong body"
      Put("/qm/" + specId + "/scenarioId_5", wrongBody) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual
          s"""Lack of parameters:
             |List(Body :
             |List(expected (body) - was ($wrongBody)))""".stripMargin
      }
    }
  }
}
